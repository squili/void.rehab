#!/bin/bash

podman run \
    --env=NODE_ENV=production \
    --mount=type=bind,source=/inpatient/files,destination=/firefish/files \
    --mount=type=bind,source=/inpatient/config,destination=/firefish/.config,ro=true \
    --mount=type=tmpfs,destination=/root/.cache \
    --mount=type=tmpfs,destination=/tmp \
    --network=host \
    --read-only \
    --rm \
    --init \
    --name inpatient \
    codeberg.org/squili/inpatient:1.0.5-rc.inpatient1
