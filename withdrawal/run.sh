#!/bin/bash

podman run \
    --env=NODE_ENV=production \
    --mount=type=bind,source=/withdrawal/files,destination=/iceshrimp/files \
    --mount=type=bind,source=/withdrawal/config,destination=/iceshrimp/.config,ro=true \
    --mount=type=tmpfs,destination=/root/.cache \
    --mount=type=tmpfs,destination=/tmp \
    --network=host \
    --read-only \
    --init \
    --rm \
    --name withdrawal \
    iceshrimp.dev/mia/withdrawal:v2023.12-pre4.withdrawal1
