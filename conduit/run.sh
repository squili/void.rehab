#!/bin/bash

podman run \
    --env=CONDUIT_CONFIG='/etc/matrix-conduit/conduit.toml' \
    --volume=conduitdb:/var/lib/matrix-conduit/ \
    --mount=type=bind,source=/conduit/conduit.toml,destination=/etc/matrix-conduit/conduit.toml \
    --network=host \
    --read-only \
    --rm \
    --name conduit \
    --no-healthcheck \
    registry.gitlab.com/famedly/conduit/matrix-conduit:next
