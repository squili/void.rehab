#!/bin/bash

podman run \
    --mount=type=bind,source=/sonic/store,destination=/store \
    --mount=type=bind,source=/sonic/sonic.cfg,destination=/etc/sonic.cfg,ro=true \
    --network=host \
    --read-only \
    --rm \
    --name sonic \
    docker.io/valeriansaliou/sonic:v1.4.1
